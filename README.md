
**checkSmartySyntax**

	tplファイルのSmarty文法チェック

---

[TOC]

---

## Usage

	検証したいtplファイルを引数に指定して起動します。
	ドラッグ＆ドロップ操作用にバッチファイルを追加しています。
	ex) php "checkSmartySyntax.php" "sample.tpl" >> sample.log 2>>&1

	文法に誤りがある場合は「PHP Fatal error」など固有のメッセージがログ(コンソール)に出力されます。
	実行例は｢sample｣フォルダに格納しています。

	必要な設定は smartyConfig.php で指定できます。


## Note

	・bat は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・Smartyでコンパイルを実行するため、実行環境に Smarty.class.php が必要になります。
	・Smarty2, Smarty3 は動作確認しています。(バージョンによって起動方法が変わります)
	・チェック実行の際、各設定のフォルダが存在しない場合は、空のフォルダを作成します。


---

## References

	かねと
	Smartyのシンタックスチェック | 備忘録
	https://ameblo.jp/kaneto-k/entry-11345483729.html

	New Digital Group, Inc
	Smarty3 マニュアル | Smarty
	https://www.smarty.net/docs/ja/index.tpl

	New Digital Group, Inc
	基本的なインストール | Smarty
	https://www.smarty.net/docs/ja/installing.smarty.basic.tpl


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
