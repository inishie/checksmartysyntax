<?php
// tplファイルのSmarty文法チェック
// (Smarty2,Smarty3対応版)

require_once(__DIR__. '/smartyConfig.php');

// 初期化処理
smartyConfig::InitializePhp();

// ファイルパスの取得
$path =  $argv[1];

// コンパイル
$smarty = new Smarty();
$compile_dir  = smartyConfig::rootFolder() . 'templates_c/' ;	// コンパイルされたテンプレート
$template_dir = smartyConfig::rootFolder() . 'templates/' ;		// テンプレートディレクトリ
$config_dir   = smartyConfig::rootFolder() . 'configs/' ;		// 設定ファイル
$cache_dir    = smartyConfig::rootFolder() . 'cache/' ;			// テンプレートのキャッシュ

mkdir($compile_dir);
mkdir($template_dir);
mkdir($config_dir);
mkdir($cache_dir);

if (method_exists($smarty, 'setCompileDir')) {
	// Smarty3
	$smarty->setCompileDir($compile_dir);
	$smarty->setTemplateDir($template_dir);
	$smarty->setConfigDir($config_dir);
	$smarty->setCacheDir($cache_dir);
} else {
	// Smarty2
	$smarty->compile_dir  = $compile_dir;
	$smarty->template_dir = $template_dir;
	$smarty->config_dir   = $config_dir;
	$smarty->cache_dir    = $cache_dir;
}
$smarty->fetch($path);
?>
