<?php
/**
 * クラス名チェック 設定
 */
class smartyConfig {

	/** プロジェクトフォルダ */
	public static function rootFolder() {
		// getcwd() // カレントのワーキングディレクトリ
		return  getcwd() . "/";
	}

	/** Smartyフォルダ */
	public static function smartyFolder() {
		return "C:/soft/smarty/";
	}

	/** 初期化処理 */
	public static function InitializePhp() {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING); // 通知, 非推奨, 警告 を非表示
		// define('DMY', null);

		self::requirePhp();
	}

	/** 事前のPHP読込 */
	public static function requirePhp() {
		// smartyCheck
		require_once(self::smartyFolder() . "Smarty.class.php");
	}

}
?>
