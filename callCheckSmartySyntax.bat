@ECHO OFF 
cd /d %~dp0

if "%~1"=="" (
	echo Please specify a tpl file.
	timeout 4
	exit /B 1
)

set YYYYMMDD=%DATE:/=%
set LOG_FILE=log-%YYYYMMDD%-%~nx1.log

echo %~nx1 > %LOG_FILE%
echo. >> %LOG_FILE%

php "checkSmartySyntax.php" "%~1" >> %LOG_FILE% 2>>&1

type %LOG_FILE%

timeout 4
exit /B 0
